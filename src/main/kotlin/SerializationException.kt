class SerializationException(val msg: String) : Exception(msg)
